function problem4(inventory) {

  let year_arr = [];
  for (let index = 0; index < inventory.length; index++) {

    year_arr.push(inventory[index].car_year);

  }
  return year_arr;
}

module.exports = problem4;