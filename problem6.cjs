function problem6(inventory) {

    bmw_audi_array = [];
    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].car_make === "BMW" || inventory[index].car_make === "Audi") {
            bmw_audi_array.push(inventory[index]);
        }
    }
    return bmw_audi_array;
}

module.exports = problem6;