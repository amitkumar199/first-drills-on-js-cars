function problem1(inventory,id)
{
    if(!Array.isArray(inventory)||inventory.length==0||typeof id!=='number'){
        return [];
    }


    for(let index=0;index<inventory.length;index++)
    {
        if(inventory[index].id===id)
        {
            return [inventory[index]];
        }
    }
    return [];
}

module.exports = problem1;


