function problem3(inventory) {
    const sort_alpha = [...inventory];
    for (let index = 0; index < sort_alpha.length - 1; index++) {
        let min_mum = index;
        for (let second_val = index + 1; second_val < sort_alpha.length; second_val++) {
            if (sort_alpha[second_val]["car_model"][0] < sort_alpha[min_mum]["car_model"][0]) {
                min_mum = second_val;
            }
        }
        let temp = sort_alpha[min_mum];
        sort_alpha[min_mum] = sort_alpha[index];
        sort_alpha[index] = temp;
    }

    return sort_alpha;
}

module.exports = problem3;