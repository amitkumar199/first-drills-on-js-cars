function problem5(inventory) {

  let year_arr = [];
  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].car_year < 2000) {
      year_arr.push(inventory[index]);
    }
  }

  return year_arr;
}

module.exports = problem5;